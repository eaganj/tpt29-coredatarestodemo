//
//  CDDResto.h
//  CoreDataRestoDemo
//
//  Created by James Eagan on 19/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CDDResto : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic) double price;
@property (nonatomic) double rating;
@property (nonatomic) NSTimeInterval lastModified;
@property (nonatomic, strong) UIImage *image;
@property (nonatomic, retain) NSData *imageData;

- (void)addOneStar;

@end

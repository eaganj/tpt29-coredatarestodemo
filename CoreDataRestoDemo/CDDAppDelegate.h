//
//  CDDAppDelegate.h
//  CoreDataRestoDemo
//
//  Created by James Eagan on 19/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CDDViewController;

@interface CDDAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) CDDViewController *viewController;

@end

//
//  CDDRestoStore.h
//  CoreDataRestoDemo
//
//  Created by James Eagan on 19/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CDDResto.h"

@interface CDDRestoStore : NSObject
{
    NSMutableArray *allRestos;
    NSManagedObjectContext *context;
    NSManagedObjectModel *model;
}

- (void)loadAllRestos;
- (NSArray *)allRestos;
- (CDDResto *)createItem;
- (NSString *)restoArchivePath;
- (BOOL)saveChanges;

@end

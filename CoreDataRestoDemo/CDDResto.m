//
//  CDDResto.m
//  CoreDataRestoDemo
//
//  Created by James Eagan on 19/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import "CDDResto.h"


@implementation CDDResto

@dynamic name;
@dynamic price;
@dynamic rating;
@dynamic lastModified;
@dynamic image;
@dynamic imageData;

- (void)awakeFromInsert
{
    [super awakeFromInsert];
    
    NSTimeInterval now = [[NSDate date] timeIntervalSinceReferenceDate];
    self.lastModified = now;
}

- (void)awakeFromFetch
{
    [super awakeFromFetch];
    
    UIImage *image = [UIImage imageWithData:self.imageData];
    [self setPrimitiveValue:image forKey:@"image"];
}

- (void)addOneStar
{
    self.rating += 1;
}

@end

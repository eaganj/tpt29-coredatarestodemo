//
//  CDDRestoStore.m
//  CoreDataRestoDemo
//
//  Created by James Eagan on 19/03/13.
//  Copyright (c) 2013 James Eagan. All rights reserved.
//

#import "CDDRestoStore.h"

@implementation CDDRestoStore

- (id)init
{
    self = [super init];
    if (!self)
        return self;
    
    // initialize store here
    model = [NSManagedObjectModel mergedModelFromBundles:nil];
    
    NSPersistentStoreCoordinator *psc =
        [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:model];
    NSURL *storeURL = [NSURL fileURLWithPath:self.restoArchivePath];
    
    NSError *error = nil;
    if (![psc addPersistentStoreWithType:NSSQLiteStoreType
                      configuration:nil
                                URL:storeURL
                            options:nil
                              error:&error])
    {
        [NSException raise:@"Could not load stored restaurants"
                    format:@"Reason: %@", error.localizedDescription];
    }
    
    context = [[NSManagedObjectContext alloc] init];
    context.persistentStoreCoordinator = psc;
    context.undoManager = nil;
    
    [self loadAllRestos];
    
    return self;
}

- (void)loadAllRestos
{
    if (!allRestos) {
        NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:@"CDDResto"];
        NSSortDescriptor *sd = [NSSortDescriptor sortDescriptorWithKey:@"name" ascending:YES];
        request.sortDescriptors = @[sd];
        
        NSError *error = nil;
        NSArray *result = [context executeFetchRequest:request error:&error];
        if (!result) {
            [NSException raise:@"Fetch failed"
                        format:@"Reason: %@", error.localizedDescription];
        }
        
        // if we're still here, all is good.
//        allRestos = [[NSMutableArray alloc] initWithArray:result]; // or:
        allRestos = [result mutableCopy];
    }
}

- (NSArray *)allRestos
{
    return allRestos;
}

- (CDDResto *)createItem;
{
    CDDResto *resto = [NSEntityDescription insertNewObjectForEntityForName:@"CDDResto"
                                                    inManagedObjectContext:context];
    [allRestos addObject:resto];
    return resto;
}


- (NSString *)restoArchivePath {
    NSArray *documentDirectories = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentDirectory = documentDirectories[0];
    return [documentDirectory stringByAppendingPathComponent:@"restos.data"];
}

- (BOOL)saveChanges {
    NSError *error = nil;
    BOOL success = [context save:&error];
    if (!success) {
        NSLog(@"Error saving: %@", error.localizedDescription);
    }
    
    return success;
}

@end
